//
//  API.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

enum GenericAPIError: Error {
    case parsingFailed
    case serverError(code: Int, message: String)
}

class API {

    var sessionManager: SessionManager!

    init(with sessionManager: SessionManager = SessionManager.default) {
        self.sessionManager = sessionManager
    }
}

// Reactive wrapper around data request
extension DataRequest {

    func responseData() -> Observable<Data> {

        return Observable.create({ (observer) -> Disposable in

            let completion: (DataResponse<Data>) -> () = ({ [weak self] (response) in

                if let statusCode = response.response?.statusCode, !(200..<300).contains(statusCode) {

                    let error = GenericAPIError.serverError(code: statusCode, message: self?.getErrorMessage(from: response.value) ?? "Unknown error. Please try again!")
                    observer.onError(error)

                } else if let data = response.value {

                    observer.onNext(data)

                }

                observer.onCompleted()

            })

            self.responseData(completionHandler: completion)

            return Disposables.create {
                self.cancel()
            }

        })
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))

    }

    func getErrorMessage(from data: Data?) -> String? {

        guard let errorData = data else {
            print("ERROR: No data found in error response!")
            return nil
        }

        let stringResponse = String(data: errorData, encoding: .utf8)

        if let data = stringResponse?.data(using: .utf8) {
            do {
                let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
                return dictionary?["message"]
            } catch {
                print("ERROR: Could not find error message with key 'message' error response!")
                return nil
            }
        }
        return nil
    }
}

extension API {

    func checkResponseValue<T>(_ parsedValue: T?) -> Observable<T> {

        return Observable.create { (observer) -> Disposable in

            guard let parsedUser = parsedValue else {
                observer.onError(GenericAPIError.parsingFailed)
                return Disposables.create()
            }

            observer.onNext(
                parsedUser
            )

            observer.onCompleted()

            return Disposables.create()

        }
    }
}

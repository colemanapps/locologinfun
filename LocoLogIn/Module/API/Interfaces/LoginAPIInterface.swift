//
//  LoginAPIInterface.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import RxSwift
import Alamofire

protocol LoginAPIInterface {
    func authenticate(with: Credentials) -> Observable<User>?
    func constructUser(from data: Data) -> Observable<User>

    var dataTask: DataRequest? { get set }
}

extension LoginAPIInterface where Self: API {

    func constructUser(from data: Data) -> Observable<User> {
        do {
            let parsedJSON = try JSONDecoder().decode(User.self, from: data)
            return self.checkResponseValue(parsedJSON)
        } catch {
            fatalError("FATAL ERROR: Parsing failed! Check the response JSON!")
        }
    }

}

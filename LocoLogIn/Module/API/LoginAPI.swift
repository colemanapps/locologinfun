//
//  UserAPI.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

final class LoginAPI: API, LoginAPIInterface {

    var dataTask: DataRequest?

    func authenticate(with credentials: Credentials) -> Observable<User>? {

        if dataTask != nil {
            dataTask?.cancel()
            return nil
        }

        let url = URLBuilder.buildAuthenticateURL()

        let parameters = [
            "email": credentials.email,
            "password": credentials.password
        ]

        dataTask = API().sessionManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)

        return dataTask?
            .responseData()
            .do(onDispose: { [weak self] in
                self?.dataTask = nil
            })
            .flatMap {
                return self.constructUser(from: $0)
            }
            .observeOn(MainScheduler.instance)

    }
}

//
//  URLBuilder.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

struct URLConstants {

    static let baseURL = "https://p0jtvgfrj3.execute-api.eu-central-1.amazonaws.com"
}

class URLBuilder {

    private var urlString: String
    private var firstPair: Bool = true

    init(with host: String) {
        self.urlString = host
    }

    func build() -> String {
        return urlString
    }
}

extension URLBuilder {

    static func buildAuthenticateURL() -> String {
        let url = URLBuilder(with: "\(URLConstants.baseURL)/test/authenticate")
            .build()
        return url
    }
}

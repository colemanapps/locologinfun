//
//  AppDelegate.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var router: RouterContract!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)

        if let tests = ProcessInfo().environment["TEST_RUNNING"], tests == "YES" {
            print("Unit tests detected, loading app UI skipped.")

            window?.rootViewController = UIViewController()
            window?.makeKeyAndVisible()
            return true
        }

        // start the flow
        router = Router(window: window!)
        router.presentLogin()
        window?.makeKeyAndVisible()

        return true
    }

}


//
//  FadeTransition.swift
//  LocoLogIn
//
//  Created by George Coleman on 15/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

open class FadeTransition: NSObject, UIViewControllerAnimatedTransitioning {

    let TRANSITION_DURATION: TimeInterval = 1
    let STARTING_ALPHA: CGFloat = 0.0

    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return TRANSITION_DURATION
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        
        guard let toView = transitionContext.view(forKey: .to), let fromView = transitionContext.view(forKey: .from) else { return }

        toView.alpha = STARTING_ALPHA
        fromView.alpha = 0.8

        let view = UIView(frame: fromView.frame)
        view.backgroundColor = UIColor(named: "LoginButtonColor")
        containerView.addSubview(view)
        containerView.addSubview(toView)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: { () -> Void in
            
            toView.alpha = 1.0
            fromView.alpha = 0.0

        }, completion: { (success) in

            fromView.alpha = 1.0
            transitionContext.completeTransition(success)
        })
    }
}

//
//  LottieView.swift
//  LocoLogIn
//
//  Created by George Coleman on 14/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import Lottie

final class LottieView: UIView {

    enum Scheme: String {
        case like
        case unlike
        case tracking
        case start_looking
        case stop_looking
    }

    var currentScheme: Scheme?

    let animationView = AnimationView(name: Scheme.like.rawValue)

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        animationView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(animationView)
        setupConstraints()
    }

    private func setupConstraints() {
        animationView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1).isActive = true
        animationView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1).isActive = true
        animationView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        animationView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }

    func set(scheme: Scheme) {
        animationView.animation = Animation.named(scheme.rawValue)
        currentScheme = scheme
    }

    // MARK: Animation
    func play(completion: LottieCompletionBlock? = nil) {
        animationView.play(completion: completion)
    }

    func stop() {
        animationView.stop()
    }

    func playCurrentAnimationInReverse(completion: LottieCompletionBlock? = nil) {
        if let animation = animationView.animation {
            animationView.play(fromFrame: animation.endFrame, toFrame: animation.startFrame, completion: completion)
        }
    }
}

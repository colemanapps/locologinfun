//
//  Credentials.swift
//  LocoLogIn
//
//  Created by George Coleman on 14/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

struct Credentials {
    public var email: String
    public var password: String
}

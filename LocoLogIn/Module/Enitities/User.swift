//
//  User.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

struct User: Decodable {
    let token: String
    let message: String
}

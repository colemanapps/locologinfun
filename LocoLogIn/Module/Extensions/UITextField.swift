//
//  UITextField.swift
//  LocoLogIn
//
//  Created by George Coleman on 16/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

extension UITextField {

    func setLeftPadding(_ amount: CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPadding(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

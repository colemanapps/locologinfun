import UIKit

protocol RouterContract {
    
    func presentLogin()
    func presentSuccess(from loginViewController: LoginViewController, by button: LoginButton)
    
}

class Router {
    
    // MARK: - Properties
    private weak var window: UIWindow?
    private var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Login", bundle: nil)
    }
    
    // MARK: - Initializers
    init(window: UIWindow) {
        self.window = window
    }

}

extension Router: RouterContract {
    
    func presentLogin() {
        let viewController = mainStoryboard.instantiateInitialViewController() as! LoginViewController
        let presenter = LoginPresenter(loginAPI: LoginAPI(), delegate: viewController)
        viewController.presenter = presenter
        window?.rootViewController = viewController
    }
    
    func presentSuccess(from loginViewController: LoginViewController, by button: LoginButton) {

        button.animate(1, completion: { () -> () in
            let viewController = self.mainStoryboard.instantiateViewController(withIdentifier: "SuccessViewController") as! SuccessViewController
            let presenter = SuccessPresenter()
            viewController.presenter = presenter
            viewController.transitioningDelegate = loginViewController
            loginViewController.present(viewController, animated: true, completion: nil)
        })
    }
}

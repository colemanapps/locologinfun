//
//  LoginPresenter.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import RxSwift

protocol LoginPresenterDelegate {
    func userLoggedInSuccessfully()
    func userLogInFailed(with error: Error)
    func userCancelledLoginAttempt()
}

class LoginPresenter {
    
    // MARK: - Properties
    var loginAPI: LoginAPIInterface!
    var user: User?
    var delegate: LoginPresenterDelegate?
    let disposeBag = DisposeBag()

    // MARK: - Initializers
    init(loginAPI: LoginAPIInterface, delegate: LoginPresenterDelegate) {
        self.loginAPI = loginAPI
        self.delegate = delegate
    }
}

extension LoginPresenter: LoginPresenterContract {

    func authenticate(with email: String, and password: String) {

        let credentials = Credentials(email: email, password: password)
        if let observableUser = loginAPI.authenticate(with: credentials) {

            observableUser.subscribe(onNext: { [weak self] user in

                guard let `self` = self else { return }
                self.user = user
                self.delegate?.userLoggedInSuccessfully()

                }, onError: { [weak self] error in

                    guard let `self` = self else { return }
                    self.delegate?.userLogInFailed(with: error)

            })
                .disposed(by: disposeBag)

        } else {
            self.delegate?.userCancelledLoginAttempt()
        }
    }

    func checkEmailForValidity(with email: String) -> Bool {

        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)

    }
}

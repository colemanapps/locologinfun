//
//  LoginPresenterContract.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

protocol LoginPresenterContract: class {
    
    func authenticate(with email: String, and password: String)
    func checkEmailForValidity(with email: String) -> Bool
}

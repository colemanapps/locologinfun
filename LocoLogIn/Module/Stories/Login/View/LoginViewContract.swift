//
//  LoginViewContract.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewContract: class {
    func setupLogin()
    func showError(_ error: Error)
    func hideError()
    func didPressLogin(_ sender: UIButton)
}

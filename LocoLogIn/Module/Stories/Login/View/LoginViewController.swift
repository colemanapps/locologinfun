//
//  LoginViewController.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum LoginViewError: Error {
    case invalidEmailError(message: String)
    case userCancelled(message: String)
}

class LoginViewController: UIViewController, LoginViewContract {

    // MARK: - Properties

    // Constants
    private let ANIMATION_DURATION: TimeInterval = 0.25

    var presenter: LoginPresenterContract!
    var router: Router!

    // Outlets
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: LoginButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loginStackView: UIStackView!
    @IBOutlet weak var lottieView: LottieView!

    private var failedLogin: Bool = false
    private var disposeBag = DisposeBag()


    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        layoutTextfields()
        layoutButton()
        setupLogin()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        setupLottieView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if let window = UIApplication.shared.keyWindow {
            router = Router(window: window)
        } else {
            assertionFailure("ERROR: No window found in viewDidAppear of LoginViewController")
        }
    }
}

extension LoginViewController {

    func setupLogin() {

        let emailObservable = emailField.rx.text.orEmpty.asObservable()
        let passwordObservable = passwordField.rx.text.orEmpty.asObservable()

        // Observe the email and password field
        Observable.combineLatest(emailObservable, passwordObservable)
            .map {
                return $0.0.count > 0 && $0.1.count > 0 && self.failedLogin == false
            }
            .bind(to: loginButton.rx.isEnabled)
            .disposed(by: disposeBag)

    }

    func setupLottieView() {

        lottieView.layer.cornerRadius = lottieView.frame.height / 2
        lottieView.layer.borderColor = UIColor.gray.cgColor
        lottieView.layer.borderWidth = 1
        lottieView.set(scheme: .start_looking)
    }
}

// Log in IBActions
extension LoginViewController {

    func showError(_ error: Error) {

        if case GenericAPIError.serverError(let code, let message) = error {
            errorLabel.text = "Something went wrong (error code \(code)): \(message)"
        } else if case LoginViewError.invalidEmailError(message: let message) = error {
            errorLabel.text = message
            emailField.layer.borderColor = UIColor.red.cgColor
        } else if case LoginViewError.userCancelled(message: let message) = error {
            errorLabel.text = message
        }

        guard errorView.isHidden else {
            return
        }

        errorLabel.alpha = 0

        UIView.animate(withDuration: ANIMATION_DURATION, animations: {
            if self.errorView.isHidden {
                self.errorView.isHidden = false
            }
            self.errorLabel.alpha = 1
        })

    }

    func hideError() {

        guard !errorView.isHidden else {
            return
        }

        errorLabel.alpha = 1

        UIView.animate(withDuration: ANIMATION_DURATION, animations: {
            if !self.errorView.isHidden {
                self.errorView.isHidden = true
            }
            self.errorLabel.alpha = 0
        })

    }

    private func layoutTextfields() {

        layoutTextfield(emailField)
        layoutTextfield(passwordField)
    }

    private func layoutTextfield(_ textField: UITextField) {

        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = textField.frame.height / 2
        textField.setLeftPadding(12)
        textField.setRightPadding(12)
    }

    private func layoutButton() {
        loginButton.layer.cornerRadius = loginButton.frame.height / 2
    }

    @IBAction func didPressLogin(_ sender: UIButton) {

        guard failedLogin == false else { return }

        hideError()

        guard let email = emailField.text,
            let password = passwordField.text else { return }

        guard presenter.checkEmailForValidity(with: email) else {
            showError(LoginViewError.invalidEmailError(message: "Please fill in a valid email address and try again!"))
            return
        }

        loginButton.setTitle("Cancel", for: .normal)

        activityIndicator.startAnimating()

        presenter.authenticate(with: email, and: password)
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }
}

extension LoginViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == emailField.tag {

            self.switchAnimation(to: .tracking)

        } else if textField.tag == passwordField.tag {

            self.switchAnimation(to: .stop_looking)

        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        // Go to password field
        if let view = loginStackView.viewWithTag(textField.tag+1) {
            view.becomeFirstResponder()
        }

        // Try logging in if on password already
        if textField.tag == passwordField.tag {
            resignFirstResponder()
            didPressLogin(loginButton)
        }

        return true

    }

    @IBAction func editingChanged(sender: AnyObject) {

        if let textField = sender as? UITextField, textField.tag == emailField.tag, lottieView.animationView.isAnimationPlaying == false {
            lottieView.animationView.currentProgress = self.getCurrentProgressForTracker()
        }

        hideError()
        failedLogin = false
        layoutTextfields()
    }
}

extension LoginViewController: LoginPresenterDelegate {

    func userCancelledLoginAttempt() {
        showError(LoginViewError.userCancelled(message: "You cancelled your login attempt. Please try again."))
        loginButton.setTitle("Try again", for: .normal)
        activityIndicator.stopAnimating()
    }

    func userLoggedInSuccessfully() {
        activityIndicator.stopAnimating()
        view.endEditing(true)
        loginButton.setTitle("Success!", for: .normal)
        router.presentSuccess(from: self, by: loginButton)
    }

    func userLogInFailed(with error: Error) {
        showError(error)
        loginButton.isEnabled = false
        failedLogin = true
        loginButton.setTitle("Try again", for: .normal)
        activityIndicator.stopAnimating()
    }
}

extension LoginViewController: UIViewControllerTransitioningDelegate {

    // MARK: UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadeTransition()
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
}

extension LoginViewController {

    func getCurrentProgressForTracker() -> CGFloat {

        if let selectedRange = emailField.selectedTextRange {

            let cursorPosition = emailField.offset(from: emailField.beginningOfDocument, to: selectedRange.start)

            if cursorPosition == 0 {
                return 0
            } else {
                let percentageOfCursorPosition: CGFloat = CGFloat(cursorPosition) / 26.0 // after 26 characters we have reached the end of the textfield

                return min(1, percentageOfCursorPosition)
            }
        }

        return 0
    }

    func switchAnimation(to scheme: LottieView.Scheme) {

        switch scheme {
        case .start_looking:

            return

        case .stop_looking:

            lottieView.set(scheme: .stop_looking)
            lottieView.play()

        case .tracking:

            let completion: (Bool) -> Void = { (isCompleted) in
                if isCompleted {

                    self.lottieView.set(scheme: .tracking)
                    self.lottieView.animationView.currentProgress = self.getCurrentProgressForTracker()
                }
            }

            if lottieView.currentScheme == .stop_looking {
                lottieView.playCurrentAnimationInReverse(completion: completion)
            } else {

                lottieView.play { (isCompleted) in
                    completion(isCompleted)
                }
            }

        default:
            return
        }


    }

}

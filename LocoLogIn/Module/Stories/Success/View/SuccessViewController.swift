//
//  SuccessViewController.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

class SuccessViewController: UIViewController, SuccessViewContract {
    
    // MARK: - Properties
    var presenter: SuccessPresenterContract!
    @IBOutlet weak var lottieView: LottieView!

    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lottieView.set(scheme: .like)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        lottieView.addGestureRecognizer(tap)
    }

    @objc func handleTap() {
        lottieView.play { (isCompleted) in
            if isCompleted {

                if self.lottieView.currentScheme == .like {
                    self.lottieView.set(scheme: .unlike)
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
                
                self.lottieView.animationView.currentProgress = 0
            }
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}

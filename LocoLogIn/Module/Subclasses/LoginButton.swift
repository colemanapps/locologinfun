//
//  LoginButton.swift
//  LocoLogIn
//
//  Created by George Coleman on 13/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//
// Based on https://github.com/vinit5320/AnimatedLogin/tree/master/AnimatedLogin

import UIKit

class LoginButton: UIButton, UIViewControllerTransitioningDelegate, CAAnimationDelegate {

    override var isEnabled: Bool {
        didSet {
            alpha = isEnabled ? 0.75 : 0.25
        }
    }

    lazy var spinner: UIActivityIndicatorView! = {
        let spinnerView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        addSubview(spinnerView)
        return spinnerView
    }()

    var didFinishAnimation : (()->())? = nil

    let springGoEase = CAMediaTimingFunction(controlPoints: 0.45, -0.36, 0.44, 0.92)
    let shrinkCurve = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
    let expandCurve = CAMediaTimingFunction(controlPoints: 0.95, 0.02, 1, 0.05)
    let shrinkDuration: CFTimeInterval  = 0.1

    var cachedTitle: String?

    func startLoadingAnimation() {
        self.cachedTitle = title(for: UIControl.State())
        self.setTitle("", for: UIControl.State())
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.layer.cornerRadius = self.frame.height / 2
        }, completion: { (done) -> Void in
            self.shrink()
            DispatchQueue.main.asyncAfter(deadline: .now() + self.shrinkDuration - 0.25) {
                self.spinner.startAnimating()
            }
        })

    }

    func startExpandAnimation(_ delay: TimeInterval, completion:(()->())?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            self.didFinishAnimation = completion
            self.expand()
            self.spinner.stopAnimating()
        }
    }

    func animate(_ duration: TimeInterval, completion:(()->())?) {
        startLoadingAnimation()
        startExpandAnimation(duration, completion: completion)
    }

    func setOriginalState() {
        self.returnToOriginalState()
        self.spinner.stopAnimating()
    }

    func animationDidStop(_ animation: CAAnimation, finished flag: Bool) {
        let basicAnimation = animation as! CABasicAnimation
        if basicAnimation.keyPath == "transform.scale" {
            didFinishAnimation?()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.returnToOriginalState()
            }
        }
    }

    func returnToOriginalState() {
        self.alpha = 0.75
        self.layer.removeAllAnimations()
        self.setTitle(self.cachedTitle, for: UIControl.State())
        self.spinner.stopAnimating()
    }

    func shrink() {
        let shrinkAnim = CABasicAnimation(keyPath: "bounds.size.width")
        shrinkAnim.fromValue = frame.width
        shrinkAnim.toValue = frame.height
        shrinkAnim.duration = shrinkDuration
        shrinkAnim.timingFunction = shrinkCurve
        shrinkAnim.fillMode = CAMediaTimingFillMode.forwards
        shrinkAnim.isRemovedOnCompletion = false
        layer.add(shrinkAnim, forKey: shrinkAnim.keyPath)
    }

    func expand() {
        let expandAnim = CABasicAnimation(keyPath: "transform.scale")
        expandAnim.fromValue = 1.0
        expandAnim.toValue = 26.0
        expandAnim.timingFunction = expandCurve
        expandAnim.duration = 0.3
        expandAnim.delegate = self
        expandAnim.fillMode = CAMediaTimingFillMode.forwards
        expandAnim.isRemovedOnCompletion = false
        self.alpha = 1
        layer.add(expandAnim, forKey: expandAnim.keyPath)
    }
}

//
//  APITests.swift
//  LocoLogInTests
//
//  Created by George Coleman on 15/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import XCTest
import RxCocoa
import RxSwift
import RxTest
import RxBlocking

@testable import LocoLogIn

class APITests: XCTestCase {
    
    private var mockAPI: API!
    private var liveAPI: API!

    struct MockJSONFiles {
        static let mockResponse = "mock_response"
        static let noVenues = "no_venues"
    }
    
    override func setUp() {
        self.liveAPI = API()
        self.mockAPI = API(with: MockSessionManager())
    }

    override func tearDown() {
        self.liveAPI = nil
        self.mockAPI = nil
    }
    
    func testLiveRequest() {
        let expect = expectation(description: "get request with live")
        
        liveAPI.sessionManager.request("https://httpbin.org/get").responseJSON { response in
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 3.0, handler: nil)
    }
    
    func testMockRequest() {
    
        let expect = expectation(description: "get request with mock")
        
        mockAPI.sessionManager.request(MockJSONFiles.mockResponse).responseJSON { response in
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testResponseDataSuccess() {
        
        let expect = expectation(description: "turn successful response into RxStream")

        _ = mockAPI.sessionManager.request(MockJSONFiles.mockResponse)
            .responseData()
            .subscribe(onNext: { data in
                expect.fulfill()
            })
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func testResponseDataError() {
        
        let expect = expectation(description: "turn failed response into RxStream")

       _ = liveAPI.sessionManager.request("https://httpbin.org/status/404")
            .responseData()
            .subscribe(onError: { (error) in
                expect.fulfill()
            })
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func testCheckResponseValueWithNil() {
      
        let nilValue: String? = nil
        
        let _ = mockAPI.checkResponseValue(nilValue)
            .subscribe(onError: { error in
                
                if case GenericAPIError.parsingFailed = error {
                    XCTAssert(true)
                } else {
                    XCTAssert(false)
                }
            })
    }

    func testGetErrorMessage() {

        let expect = expectation(description: "turn emprty data into RxStream")

        _ = liveAPI.sessionManager.request("https://httpbin.org/status/404")
            .responseData()
            .subscribe(onError: { (error) in
                if case GenericAPIError.serverError(code: 404, message: "Unknown error. Please try again!") = error {
                    expect.fulfill()
                }
            })

        waitForExpectations(timeout: 1.0, handler: nil)
    }

}

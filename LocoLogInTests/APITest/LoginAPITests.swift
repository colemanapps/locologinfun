//
//  LoginAPITests.swift
//  LocoLogInTests
//
//  Created by George Coleman on 14/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import XCTest
import Alamofire

@testable import LocoLogIn

class LoginAPITests: XCTestCase {

    private var loginAPI: LoginAPI!
    private var mockLoginAPI: MockLoginAPI!

    private var dataTask: DataRequest?
    private let testCredentials = Credentials(email: "testemail@gmail.com", password: "test123")

    override func setUp() {
        self.loginAPI = LoginAPI()
        self.mockLoginAPI = MockLoginAPI()

    }

    override func tearDown() {
        self.loginAPI = nil
        self.mockLoginAPI = nil
    }

    func testAuthenticate() {
        
        let expect = expectation(description: "LiveAPI is working for authenticate")

        _ = loginAPI.authenticate(with: testCredentials)?
            .subscribe(onNext: { (user) in
                print("SUCCESS RESPONSE IN TEST")
                expect.fulfill()
            }, onError: { (err) in
                print("ERROR RESPONSE IN TEST")
                expect.fulfill()
            })

        waitForExpectations(timeout: 3.0, handler: nil)
    }

    func testCancelRequest() {

        self.loginAPI.dataTask = API().sessionManager.request("http://www.apple.com")

        let cancelledDataTask = self.loginAPI.authenticate(with: testCredentials)

        XCTAssertNil(cancelledDataTask)
    }

    func testConstructUserFromNilData() {

        expectFatalError(expectedMessage: "FATAL ERROR: Parsing failed! Check the response JSON!") {
            _ = self.mockLoginAPI.authenticateWithFaultyResponse()
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

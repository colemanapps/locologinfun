//
//  MockLoginAPI.swift
//  LocoLogInTests
//
//  Created by George Coleman on 15/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import RxSwift
import Alamofire

@testable import LocoLogIn

class MockLoginAPI: API, LoginAPIInterface {

    var dataTask: DataRequest?

    private var mockAPI: API = API(with: MockSessionManager())

    struct MockJSONFiles {
        static let mismatchResponse = "failure_mismatch_response"
        static let serverErrorResponse = "failure_server_error_response"
        static let successResponse = "success_response"
        static let faultySuccessResponse = "faulty_success_response"
    }

    func authenticate(with credentials: Credentials) -> Observable<User>? {

        if dataTask != nil {
            dataTask?.cancel()
            return nil
        }
        
        if credentials.email == "" || credentials.password == "" {

            return mockAPI.checkResponseValue(nil)

        } else if credentials.email == "test401" {

            return errorCase(with: (mockAPI.sessionManager as! MockSessionManager).returnJsonDataSynchronously(for: MockJSONFiles.mismatchResponse), and: 401)

        } else if credentials.email == "test500" {

            return errorCase(with: (mockAPI.sessionManager as! MockSessionManager).returnJsonDataSynchronously(for: MockJSONFiles.serverErrorResponse), and: 500)
            
        } else {

            return (mockAPI.sessionManager as! MockSessionManager).returnJsonDataSynchronously(for: MockJSONFiles.successResponse)
                .flatMap {
                    return self.constructUser(from: $0)
                }!
                .observeOn(MainScheduler.instance)
        }
    }

    func authenticateWithFaultyResponse() -> Observable<User>? {

        return (mockAPI.sessionManager as! MockSessionManager).returnJsonDataSynchronously(for: MockJSONFiles.faultySuccessResponse)
            .flatMap {
                return self.constructUser(from: $0)
            }!
            .observeOn(MainScheduler.instance)
    }

    private func errorCase(with data: Data?, and statusCode: Int) -> Observable<User>? {

        return Observable.create({ (observer) -> Disposable in

            let error: GenericAPIError!

            switch statusCode {
                case 401:
                    error = GenericAPIError.serverError(code: 401, message: "We couldn't find you in our system! Check your credentials and please try again.")
                case 500:
                    error = GenericAPIError.serverError(code: 500, message: "Something went wrong. Please try again.")
                default:
                    error = GenericAPIError.serverError(code: 0, message: "Unknown error. Please try again!")
            }

            observer.onError(error)

            return Disposables.create()
        })
    }
}

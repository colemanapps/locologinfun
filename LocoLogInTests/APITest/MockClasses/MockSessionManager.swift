//
//  MockSessionManager.swift
//  LocoLogInTests
//
//  Created by George Coleman on 15/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import Alamofire

@testable import LocoLogIn

class MockSessionManager: SessionManager {
   
    init() {
        
        let configuration: URLSessionConfiguration = {
            let configuration = URLSessionConfiguration.default
            configuration.protocolClasses = [MockURLProtocol.self]
            return configuration
        }()
        
        super.init(configuration: configuration)
    }
    
    func returnJsonDataSynchronously(for fileName: String) -> Data? {
        return MockURLProtocol.getDataFile(or: fileName)
    }
}

//
//  LoginPresenterTests.swift
//  LocoLogInTests
//
//  Created by George Coleman on 14/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import XCTest
import MapKit
import RxSwift
import RxCocoa

@testable import LocoLogIn

class LoginPresenterTests: XCTestCase {

    private var loginPresenter: LoginPresenter!
    private var mockLoginAPI: MockLoginAPI!
    private var mockAPI: API!

    override func setUp() {
        mockLoginAPI = MockLoginAPI()
        mockAPI = API(with: MockSessionManager())
        loginPresenter = LoginPresenter(loginAPI: mockLoginAPI, delegate: self)
    }

    override func tearDown() {
        loginPresenter = nil
        mockLoginAPI = nil
        mockAPI = nil
    }

    func testAuthenticateSuccess() {
        loginPresenter.authenticate(with: "email", and: "password")
    }

    func testAuthenticate401() {
        loginPresenter.authenticate(with: "test401", and: "password")
    }

    func testAuthenticate500() {
        loginPresenter.authenticate(with: "test500", and: "password")
    }

    func testAuthenticateWithEmptyEmail() {
        loginPresenter.authenticate(with: "", and: "password")
    }

    func testAuthenticateWithEmptyPassword() {
        loginPresenter.authenticate(with: "email", and: "")
    }

    func testAuthenticateWithRunningRequest() {
        mockLoginAPI.dataTask = API().sessionManager.request("http://www.apple.com")
        loginPresenter.authenticate(with: "email", and: "password")
    }

    func testCheckEmailForValidityWithWrongEmail() {
        let failedEmailCheck = loginPresenter.checkEmailForValidity(with: "tester@@gmail.com")
        XCTAssertTrue(failedEmailCheck == false)
    }

    func testCheckEmailForValidityWithEmptyString() {
        let failedEmailCheck = loginPresenter.checkEmailForValidity(with: "")
        XCTAssertTrue(failedEmailCheck == false)
    }

    func testCheckEmailForValidityWithRealEmail() {
        let successEmailCheck = loginPresenter.checkEmailForValidity(with: "tester@gmail.com")
        XCTAssertTrue(successEmailCheck)
    }
}

extension LoginPresenterTests: LoginPresenterDelegate {
    func userLoggedInSuccessfully() {
        XCTAssert(true)
    }

    func userLogInFailed(with error: Error) {
        XCTAssert(true)
    }

    func userCancelledLoginAttempt() {
        XCTAssert(true)
    }
}
